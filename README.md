# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Libraries used in this project: ###

- Bootstrap
- Jquery
- Jquery Easing
- Font Awesome
- Dev Icon

### Main classes: ###

- main-image "to make image is fullscreen image"
- link-to-about "to make button moving repeatedly top and bottom by using key frame css3"
- main-section "To make main section in the center nearly"
- other classes from bootstrap

### Main Functions: ###

- Smooth scrolling using jQuery easing when a scroll trigger link is clicked "Applied in 'js-scroll-trigger' class"
- Close responsive menu when a scroll trigger link is clicked
- Add active class to navbar items on scroll "Target id 'mainNave'"

Thanks

